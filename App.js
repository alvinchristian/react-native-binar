import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions,
  SafeAreaView,
} from 'react-native';

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.card}>
        <Image
          style={styles.image}
          resizeMode="cover"
          source={{
            uri: 'https://res.cloudinary.com/djyjm9ayd/image/upload/v1643249117/19025216_1439860629412992_3671167199250762358_o_hzryz8.png',
          }}
        />
        <View>
          <Text style={styles.title}>Styling di React Native</Text>
          <Text style={styles.subTitle}>Binar Academy - React Native</Text>
          <Text style={styles.content}>
            As a component grows in complexity, it is much cleaner and efficient
            to use StyleSheet.create so as to define several in one place.
          </Text>
          <View style={styles.footer}>
            <Text style={styles.response}>Understood!</Text>
            <Text style={styles.response}>What?!!</Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 10,
    marginTop: 50,
    width: screen.width * 0.9,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.3,
    shadowRadius: 10,
    elevation: 10,
  },
  image: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    height: screen.width * 0.8,
  },
  title: {
    marginTop: 14,
    fontSize: 18,
    alignSelf: 'center',
    color: '#000',
  },
  subTitle: {
    alignSelf: 'center',
    color: 'grey',
    fontSize: 14,
  },
  content: {
    marginTop: 5,
    marginBottom: 25,
    textAlign: 'justify',
    fontSize: 14,
    marginHorizontal: 20,
    color: '#000',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 35,
    marginBottom: 40,
  },
  response: {
    color: 'green',
    fontSize: 18,
  },
});
